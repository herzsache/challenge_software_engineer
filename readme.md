# Challenge

## The goals
Develop a backend platform that can safely and reliably perform the following features.

* signup
* login
* list users
* add portfolio
* delete portfolio
* list portfolio
* portfolio balance
* add trade
* delete trade
* list trades

## Logic
The user has one or more portfolios, each portfolio has a list of trades, and the sum of all trades gives you the portfolio position.

Hint: A trade that _buys_ the asset _cash_, is a positive cash flow into the portfolio.

## Model examples

### User model
	{
	    "id": "aa43d7ed-a03b-478d-a061-7411ddca000f",
	    "created": "2019-08-26T01:40:01.350529+00:00",
	    "modified": "2019-08-29T05:58:29.022719+00:00",
	    "username": "my_username",
	    "password": "xSubEpwq3r0KGpXfoq05ylY6dDfT/HgBUrqL0JMsXy4=",
	    "name": "my_first_name my_last_name",
	    "email": "my_email@email.com",
	    "salt": null,
	    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2",
	    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8v",
	    "token_expiration": "2019-09-10T20:25:55Z",
	    "refresh_token_expiration": "2019-09-11T20:25:56Z"
	}

### Trade model
	{
		"id": "aa43d7ed-a03b-478d-a061-7411ddca000f",
		"created": "2019-08-26T01:40:01.350529+00:00",
		"modified": "2019-08-29T05:58:29.022719+00:00",
		"user_executor": "aa43d7ed-a03b-478d-a061-7411ddca000f",
		"portfolio": "aa43d7ed-a03b-478d-a061-7411ddca000f",
		"date": "2019-08-25",
		"number_of_shares": 15,
		"price": 15.48,
		"currency": "USD",
		"market_value": 232.20,
		"action": "buy",
		"notes": null,
		"asset": "PT10Y"
	}

## The tools
You can use whatever tools and programing languages you feel will provide a good solution for your problem.

## The means
Run the platform on Amazon AWS. For example (but not mandatory) EC2/ElasticBeanstalk/Lambda/RDS.

Please let us know if there is any resource you need for you project that is locked on your account and we will make sure we give you access and permissions to all the tools you need.

Log in in on the AWS with the account ID **476638174028** on [http://aws.amazon.com](http://aws.amazon.com)

![Image](login_01.png "aws_login_01")

After you press next, you will be asked for you IAM username and password, witch was provided to you.
**If you don't have login credentials, please reach out.** 

![Image](login_02.png "aws_login_02")


## The expected results
You are expected to deliver a platform where all the required features work, are secure, reliable and scalable.



**If you have any doubts, or you can't figure out a part for process, please reach out, we are here to work as a team.**


## Final comments
This challenge is not meant to be done in a specific time window, take your time to architect, develop and even refactor your code.

Once you have your project finished and you are confortable, please reach out so we can send you the instructions for you to push your project to a private repository.